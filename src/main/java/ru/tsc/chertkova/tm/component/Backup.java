package ru.tsc.chertkova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.command.domain.AbstractDomainCommand;
import ru.tsc.chertkova.tm.command.domain.DomainBackupLoadCommand;
import ru.tsc.chertkova.tm.command.domain.DomainBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.processCommand(DomainBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDomainCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DomainBackupLoadCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            Thread.sleep(3000);
            if (!bootstrap.getAuthService().isAuth()) save();
        }
    }

}
